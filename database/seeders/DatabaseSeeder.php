<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use App\Permission\UserPermission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Permission::create(['name' => "post_add"]);
        // Permission::create(['name' => "post_delete"]);
        // Permission::create(['name' => "post_edit"]);
        // Permission::create(['name' => "all_post"]);
        // Permission::create(['name' => "create_user"]);

        User::factory(10)->create()->each(function($user){
            $per = UserPermission::all();
            foreach($per as $key => $p){
                 foreach($p as $name => $single){
                    $per[$key][$name] = rand(0,1) == 0 ? true : false;
                 }
            }
            $user->permission = json_encode($per);
            $user->save();
        });
    }
}
