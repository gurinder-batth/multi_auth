<?php

namespace App\Models;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserPermission extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','permission_id'];

    public function permission()
    {
        return $this->hasOne(Permission::class,"id","permission_id");
    }
}
