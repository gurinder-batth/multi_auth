<?php

namespace App\Models;

use App\Models\Permission;
use App\Models\UserPermission;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function permissions()
    {
        return $this->hasMany(UserPermission::class,"user_id","id");
    }

    public function hasPermission(string $req_per) 
    {
        $per = json_decode($this->permission);
        if($per == null){
            return false;
        }
        $item =  explode(".",$req_per);
        if(isset($per->{$item[0]}->{$item[1]})){
             if( $per->{$item[0]}->{$item[1]} == true){
                 return true;
             }
        }
        return false;
    }

    public function storeOtp(int $otp)
    {
        $data = [];
        $data['otp'] = $otp;
        $data['created_at'] = now()->toDateTimeString();
        $data['attempt'] = 0;
        $this->otp = json_encode($data);
        $this->save();
    }
}
