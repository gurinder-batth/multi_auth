<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\LoginWithOtp;
use Illuminate\Http\Request;
use App\Jobs\ForgetPasswordEmailJob;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function login(Request $req)
    {
        if(Auth::attempt(['email' => $req->email , 'password' => $req->password])){
            return redirect()->route("user.main");
        }
        return redirect()->back();
    }

    public function main(Request $req)
    {
       $user = new User();
       return view("main");
    }

    public function loginWithOtp(Request $req)
    {
       return view("login.otp");
    }

    public function loginOtp(Request $req)
    {
        $user = User::whereEmail($req->email)->first();
        if($user == null){
            /**
             * TODO : session message 
             */
            return redirect()->back();
        }
        $otp = rand(10000,99999);
        $user->storeOtp($otp);
        ForgetPasswordEmailJob::dispatch($user,$otp);
        return redirect()->route("user.otp.check",["id" => $user->id]);
    }

    public function otpCheck(Request $req)
    {
        $user = User::find($req->id);
        return view("login.otp_verify")->withUser($user);
    }
    public function verifyOTP(Request $req)
    {
        $user = User::find($req->id);
        if($user->otp == null){
            return redirect()->route("login-with-otp");
        }
        /**
         * TODO: 30 Minutes 
         * TODO: 3 attempt
         */
        $user_otp = json_decode($user->otp);
        if($req->otp == $user_otp->otp){
            Auth::loginUsingId($user->id);
            return redirect()->route("user.main");
        }
        /**
         * TODO: create seesion flash message
         */
        return redirect()->back();
    }
}
