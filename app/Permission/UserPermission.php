<?php

namespace App\Permission;

class UserPermission
{
    public static function all(){
         return [
                "post" => [
                     "create" => false ,
                     "edit" => false ,
                     "delete" => false ,
                     "all_list" => false ,
                 ] ,
                "user" => [
                     "create" => false ,
                     "edit" => false ,
                     "delete" => false ,
                     "list" => false ,
                     "all_list" => false ,
                 ] ,
         ];
    }
}
