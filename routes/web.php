<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name("user");

Route::post("login",[UserController::class,'login'])->name("user.login");
Route::get("login-with-otp",[UserController::class,'loginWithOtp'])->name("user.login.otp");
Route::post("login-otp",[UserController::class,'loginOtp'])->name("user.otp.request");
Route::get("otp-check/{id}",[UserController::class,'otpCheck'])->name("user.otp.check");
Route::post("verify-otp",[UserController::class,'verifyOTP'])->name("user.verify.otp");

Route::middleware(['is_user'])->group(function(){

   
    Route::get("main",[UserController::class,'main'])->name("user.main");
    
    Route::prefix("post")->group(function(){
          Route::name("post.")->group(function(){
            Route::get("add",[PostController::class,"add"])->name("add")->middleware("is_special_user:post.create");
            Route::post("create",[PostController::class,"create"])->name("create");
            Route::get("edit/{id}",[PostController::class,"edit"])->name("edit");
            Route::post("update",[PostController::class,"update"])->name("update");
            Route::get("list",[PostController::class,"list"])->name("list");
          });
    });
    
    Route::prefix("user")->group(function(){
          Route::name("user.")->group(function(){
            Route::get("add",[UserController::class,"add"])->name("add");
            Route::get("create",[UserController::class,"create"])->name("add");
          });
    });
    
    
});